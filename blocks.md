# Blocks

A chart for if an object is referred to by name.

<style>
    img {
        width: 80px !important;
        height: 80px !important;
    }
</style>

| | | | | | | | | | | | | | | |
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
| ![Wood](images/blocks/wood.png) | ![Wood 5](images/missing.png) | ![Honey](images/blocks/honey.png) | ![Mini Bomb](images/blocks/mini_bomb.png) | ![L Girder](images/blocks/l_girder.png) | ![Crumbling Block Z](images/missing.png) | ![Tennisball Shooter](images/blocks/tennisball_shooter.png) | ![Crossbow](images/blocks/crossbow.png) | ![Paper Airplane Shooter](images/blocks/paper_airplane_shooter.png) | ![Punching Flower](images/blocks/punching_flower.png) | ![Teleporter](images/blocks/teleporter.png) | ![One-Way Door](images/blocks/one-way_door.png) | ![Hydrant](images/blocks/hydrant.png) | ![Blue Platform](images/blocks/blue_platform.png) | ![Gear](images/blocks/gear.png) |
| Wood | Wood 5 | Honey | Mini Bomb | L Girder | Crumbling Block Z | Tennisball Shooter | Crossbow | Paper Airplane Shooter | Punching Flower | Teleporter | One-Way Door | Hydrant | Blue Platform | Gear |
| ![Wood 2](images/missing.png) | ![Wood 6](images/missing.png) | ![Coin](images/blocks/coin.png) | ![Mega Bomb](images/blocks/mega_bomb.png) | ![Stairs](images/blocks/stairs.png) | ![Crumbling Block L](images/missing.png) | ![Wrecking Ball](images/blocks/wrecking_ball.png) | ![Boxing Glove](images/blocks/boxing_glove.png) | ![Cannon](images/blocks/cannon.png) | ![Beehive](images/blocks/beehive.png) | ![Barbed Wire](images/blocks/barbed_wire.png) | ![Fan Platform](images/blocks/fan_platform.png) | ![Tire](images/blocks/tire.png) | ![Ferris Wheel](images/blocks/ferris_wheel.png) | ![Purple Platform](images/blocks/purple_platform.png) |
| Wood 2 | Wood 6 | Coin | Mega Bomb | Stairs | Crumbling Block L | Wrecking Ball | Boxing Glove | Cannon | Beehive | Barbed Wire | Fan Platform | Tire | Ferris Wheel | Purple Platform |
| ![Wood 3](images/missing.png) | ![Barrel](images/blocks/barrel.png) | ![Slowatch](images/blocks/slowatch.png) | ![Jetpack](images/blocks/jetpack.png) | ![Scaffold](images/blocks/scaffold.png) | ![Spikeball](images/missing.png) | ![Spinning Saw](images/blocks/spinning_saw.png) | ![Linear Saw](images/blocks/linear_saw.png) | ![Hockey Shooter](images/blocks/hockey_shooter.png) | ![Flamethrower](images/blocks/flamethrower.png) | ![Ice](images/blocks/ice.png) | ![Barbed Wire 2](images/missing.png) | ![Barbed Wire 3](images/missing.png) | ![Treadmill](images/blocks/treadmill.png) | ![Red Platform](images/blocks/red_platform.png) |
| Wood 3 | Barrel | Slowatch | Jetpack | Scaffold | Spikeball | Spinning Saw | Linear Saw | Hockey Shooter | Flamethrower | Ice | Barbed Wire 2 | Barbed Wire 3 | Treadmill | Red Platform |
| ![Wood 4](images/missing.png) | ![Hay Bale](images/blocks/hay_bale.png) | ![Bomb](images/blocks/bomb.png) | ![Jetpack Dispenser](images/blocks/jetpack_dispenser.png) | ![Crumbling Block](images/blocks/crumbling_block.png) | ![Flipping Block](images/blocks/flipping_block.png) | ![Trigger Spikes](images/blocks/trigger_spikes.png) | ![Magnet Platform](images/blocks/magnet_platform.png) | ![Black Hole](images/blocks/black_hole.png) | ![Spring](images/blocks/spring.png) | ![Door](images/blocks/door.png) | ![Ice 2](images/missing.png) | ![Ice 3](images/missing.png) | ![Fan](images/blocks/fan.png) |
| Wood 4 | Hay Bale | Bomb | Jetpack Dispenser | Crumbling Block | Flipping Block | Trigger Spikes | Magnet Platform | Black Hole | Spring | Door | Ice 2 | Ice 3 | Fan |