# Boomer Mode

The main idea for Boomer mode is to soar through the air and dodge obstacles mid flight. The focus of the game, the main component is the cannon. Do try to place as many of them as possible, but also don't forget to put traps if needed.

## Block probabilities

| | | | | | | | | | | | | | | | | | | |
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|🟥|🟥|1️⃣|1️⃣| |1️⃣|1️⃣|1️⃣|1️⃣| |1️⃣|🟥|1️⃣|🟥| |🟥|🟥|1️⃣|⬛|
|🟥|1️⃣|🟥|🟥| |1️⃣|1️⃣|1️⃣|🟥| |5️⃣|1️⃣|🟥|🟥| |1️⃣|1️⃣|1️⃣|⬛|
|🟥|🟥|🟥|🟥| |🟥|🟥|1️⃣|1️⃣| |1️⃣|🟥|🟥|🟥| |🟥|🟥|🟥|⬛|
|🟥|🟥|🟥|🟥| |🟥|🟥|🟥|🟥| |1️⃣|🟥|🟥|🟥| |🟥|1️⃣|⬛|⬛|