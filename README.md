# Ultimáte Kikken Horce

## Introduction

This is a collection of fun and interesting game modes and play styles for [Ultimate Chicken Horse](https://store.steampowered.com/app/386940/Ultimate_Chicken_Horse/).
It's designed by some of the people of the private Matrix community the Master Branch. We play it from time to time and we have the most wonderful of experiences together. **Ultimáte Kikken Horce** ([ultimaːtɛ kikɛn horkɛ](http://ipa-reader.xyz/?text=ultima%CB%90t%C9%9B%20kik%C9%9Bn%20hork%C9%9B&voice=Carla)) is what we like to call it.

### Maintainers

It's a collaborative effort of the group to figure out what works best, but the Git repository itself is maintained solely by [deepadmax](https://deepadmax/) and [pixelized](https://ischa.dev/).

## Game Modes

- [Boomer Mode](modes/boomer.md)
- [ADHD Mode](modes/adhd.md)

## Block probabilities

According to the standard order of objects in the Block Probabilities menu, every page shall be included in a configuration as a table, as shown below.

| | | | | | | | | | | | | | | | | | | |
|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
|🟥|1️⃣|2️⃣|3️⃣| |🟥|1️⃣|2️⃣|3️⃣| |🟥|1️⃣|2️⃣|3️⃣| |🟥|1️⃣|2️⃣|⬛|
|7️⃣|6️⃣|5️⃣|4️⃣| |7️⃣|6️⃣|5️⃣|4️⃣| |7️⃣|6️⃣|5️⃣|4️⃣| |5️⃣|4️⃣|3️⃣|⬛|
|8️⃣|9️⃣|🟥|1️⃣| |8️⃣|9️⃣|🟥|1️⃣| |8️⃣|9️⃣|🟥|1️⃣| |6️⃣|7️⃣|8️⃣|⬛|
|5️⃣|4️⃣|3️⃣|2️⃣| |5️⃣|4️⃣|3️⃣|2️⃣| |5️⃣|4️⃣|3️⃣|2️⃣| |🟥|9️⃣|⬛|⬛|

## Miscellaneous

- [Chart of block names](blocks.md)