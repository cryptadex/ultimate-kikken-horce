#!python

import requests
import wget
import os

from contextlib import suppress

from bs4 import BeautifulSoup



def download(url):
    while True:
        with suppress(requests.exceptions.ConnectionError):
            return requests.get(url).content


def get_names():
    url = 'https://ultimate-chicken-horse.fandom.com/wiki/Category:Objects'

    soup = BeautifulSoup(download(url), 'html.parser')

    res = soup.find(class_='category-page__members')
    res = res.find_all(class_='category-page__member-link')

    return [tag.contents[0] for tag in res]


def get_assets(dest='./images/{name}.png'):
    names = get_names()

    for name in names:
        transcoded = name.replace(' ', '_')
        url = f'https://ultimate-chicken-horse.fandom.com/wiki/{transcoded}'
        soup = BeautifulSoup(download(url), 'html.parser')

        src = soup.find(class_='pi-image-thumbnail')['src']
        destination = dest.format(name=transcoded.lower())

        print(f'Downloading {src} into {destination}...')

        with suppress(FileNotFoundError):
            os.remove(destination)

        wget.download(src, destination)



if __name__ == '__main__':
    get_assets('../images/blocks/{name}.png')
