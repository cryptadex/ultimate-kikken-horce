import os
import sys
from pathlib import Path
from collections import defaultdict

from block_positions import blocks
from generate_table import DEFAULT_IMAGE, get_names_and_paths


# It's just a script, bro!
_root = Path(__file__).parent.parent
os.chdir(_root)


template_whole = """
<html>
    <style>
        :root {{
            --img-1-1: url("/mnt/Newton/repos/ultimate-kikken-horce/images/blocks/crate.png");
            --bg: rgb( 27,  31,  56);
            --0:  rgb( 74,  42,  56);
            --1:  rgb(166,  68, 186);
            --2:  rgb(148,  98, 195);
            --3:  rgb(107,  93, 195);
            --4:  rgb( 75,  87, 195);
            --5:  rgb( 89, 122, 195);
            --6:  rgb( 42, 122, 195);
            --7:  rgb( 53, 149, 195);
            --8:  rgb( 84, 175, 195);
            --9:  rgb(132, 187, 195);
        }}
        table {{
            border-spacing: 2px !important;
            border-collapse: separate !important;
        }}
        th {{
            display: none;
        }}
        tr td:empty {{
            padding: 0;
            min-width: 2ch;
            background: transparent;
            border: none;
        }}
        td::after {{
            content: "";
            background-size: 100% 100%;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            position: absolute;
        }}
        td {{
            position: relative;
            border-radius: 1ch;
            --color: var(--0);
            --frac: 9;
            --percentage: calc(100% - var(--frac) * (100% / 9));
            background: linear-gradient(
                var(--bg) var(--percentage), 
                var(--color) var(--percentage)
            );
            color: transparent;
        }}
        {generated_css}
    </style>
</html>
"""


template_bg = """
tr:nth-child({y}) td:nth-child({x})::after {{
    background-image: url("{image_path}");
}}
""".strip()

template_frac = """
h1[id^="{block}"][id$="{frac}"] ~table tr:nth-child({y}) td:nth-child({x}) {{
    --color: var(--{frac});
    --frac: {frac};
}}
""".strip()

inv_map = dict(zip(blocks.values(), blocks.keys()))  # Coordinates to blocks
path_map = defaultdict(lambda: DEFAULT_IMAGE, zip(*get_names_and_paths("images/blocks")))  # Blocks to paths


def css_for_coord(x, y):
    if (x, y) not in inv_map:
        return

    block = inv_map[(x, y)]
    style = template_bg.format(x=x+1, y=y+1, image_path=str(Path(path_map[block]).absolute()))

    for frac in range(10):
        style += f'\n{template_frac.format(block=block.replace(" ", "-").lower(), x=x+1, y=y+1, frac=frac)}'

    return style


def css_for_all():
    return "\n".join(css_for_coord(x, y) for x, y in inv_map.keys())


if __name__ == "__main__":
    generated_css = css_for_all()
    # generated_css = template_whole.format(generate_css='\n'.join(" " * 8 + l for l in generate_css.split('\n')))
    generated_css = template_whole.format(generated_css=generated_css)
    sys.stdout.write(generated_css)