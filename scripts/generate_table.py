import os
import sys

from rich import print
from pathlib import Path
from collections import defaultdict

from block_positions import blocks, grid


DEFAULT_IMAGE = "images/missing.png"


def get_names_and_paths(path):
    path = Path(path)

    names = []
    paths = []

    for fname in os.listdir(path):
        # Get full path to file
        fpath = path / fname
        paths.append(fpath)

        # Extract block name from file name
        name = fpath.stem.replace("_", " ").title()
        names.append(name)

    return names, paths


def asimage(alt, path):
    return f"![{alt}]({path})"


def generate_markdown_table(names, paths):
    inv_map = dict(zip(blocks.values(), blocks.keys()))
    path_map = defaultdict(lambda: DEFAULT_IMAGE, zip(names, paths))

    width, height = grid.shape

    table = "| " * width + "|\n" + \
            "|-" * width + "|\n"
    rows = []

    images = []
    captions = []

    for y in range(height):
        images.append([])
        captions.append([])

        for x in range(width):
            if (x, y) not in inv_map:
                continue
            
            name = inv_map[(x, y)]
            path = path_map[name]

            images[-1].append(asimage(name, path))
            captions[-1].append(name)

        rows.append(f'| {" | ".join(row for row in images[-1])} |')
        rows.append(f'| {" | ".join(row for row in captions[-1])} |')

    table += '\n'.join(rows)
    return table


if __name__ == "__main__":
    _root = Path(__file__).parent.parent
    os.chdir(_root)
    table = generate_markdown_table(*get_names_and_paths("images/blocks"))
    sys.stdout.write(table)
