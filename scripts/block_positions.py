import numpy as np

from pathlib import Path

_file_path = Path(__file__)
path = _file_path.parent / "grid.txt"

grid = np.genfromtxt(path, delimiter=",", dtype=str)

blocks = dict()

for x, col in enumerate(grid):
    for y, name in enumerate(col):
        if name:
            blocks[name.strip()] = (x, y)
